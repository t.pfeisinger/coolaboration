const { buildSchema } = require('graphql');
module.exports = buildSchema(`

type RegData {
    userId: ID!
    token: String!
    expiration: Int!
}

type AuthData {
    userId: ID!
    token: String!
}

input UserInput {
    email: String!
    username: String!
}

type RootQuery {
    getUsers: String!
    login(email: String!, password: String!): AuthData!
}

type RootMutation {
    createUser(userInput: UserInput): RegData!
}

schema {
    query: RootQuery
    mutation: RootMutation
}
`);