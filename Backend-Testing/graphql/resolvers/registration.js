const jwt = require('jsonwebtoken');
const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);

module.exports = {
    createUser: async args => {
        try {
            let existingUser = await User.findOne({ where: { emailAddress: args.userInput.email } });
            if(existingUser)
                throw new Error("Email already registered!");
            existingUser = await User.findOne({ where: { username: args.userInput.username } });
            if(existingUser)
                throw new Error("Username taken!");
            
            const user = await User.build({ 
                username: args.userInput.username,
                emailAddress: args.userInput.email,
                stateLabel: "REGISTERED",
                password_hash: "test"
            });
            const result = await user.save();

            console.log(result);

            const token = jwt.sign({userId: result.userID, userState: result.stateLabel}, 'tokenkey', {
                expiresIn: '1h'
            });

            return {userId: result.userID, token: token, expiration: 1};
        }
        catch (err) { throw err }
    },
    getUsers: () => {
        return "test";
    }
}