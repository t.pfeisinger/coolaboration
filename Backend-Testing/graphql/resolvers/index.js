const regResolver = require('./registration');
const logResolver = require('./login');

const rootResolver = {
  ...regResolver,
  ...logResolver
};

module.exports = rootResolver;