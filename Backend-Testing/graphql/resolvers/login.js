const jwt = require('jsonwebtoken');
const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const bcrypt = require('bcryptjs');

module.exports = {
    login: async args => {
        try {
            const user = await User.findOne({ where: { emailAddress: args.email } });
            if(!user){
                throw new Error('User does not exist!');
            }
            const isEqual = await bcrypt.compare(args.password, User.password_hash);
            if(!isEqual){
                throw new Error('Password is incorrect!');
            }
            const token = jwt.sign({userId: User.userID, email: User.emailAddress}, 'testkey');
            return { userId: user.id, token: token }
        }
        catch (err) { throw err }
    }
}
