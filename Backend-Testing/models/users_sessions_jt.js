const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('users_sessions_jt', {
    userID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'users_bt',
        key: 'userID'
      }
    },
    sessionID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'sessions',
        key: 'sessionID'
      }
    },
    roleLabel: {
      type: DataTypes.STRING(15),
      allowNull: false,
      references: {
        model: 'e_user_role',
        key: 'roleLabel'
      }
    }
  }, {
    sequelize,
    tableName: 'users_sessions_jt',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "userID" },
          { name: "sessionID" },
        ]
      },
      {
        name: "fk_USERS_BT_has_SESSIONS_SESSIONS1_idx",
        using: "BTREE",
        fields: [
          { name: "sessionID" },
        ]
      },
      {
        name: "fk_USERS_BT_has_SESSIONS_USERS_BT1_idx",
        using: "BTREE",
        fields: [
          { name: "userID" },
        ]
      },
      {
        name: "fk_USERS_SESSIONS_JT_E_USER_ROLE1_idx",
        using: "BTREE",
        fields: [
          { name: "roleLabel" },
        ]
      },
    ]
  });
};
