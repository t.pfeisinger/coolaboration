const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sessions_modules_jt', {
    sessionID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'sessions',
        key: 'sessionID'
      }
    },
    moduleID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'modules',
        key: 'moduleID'
      }
    }
  }, {
    sequelize,
    tableName: 'sessions_modules_jt',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sessionID" },
          { name: "moduleID" },
        ]
      },
      {
        name: "fk_SESSIONS_has_MODULES_MODULES1_idx",
        using: "BTREE",
        fields: [
          { name: "moduleID" },
        ]
      },
      {
        name: "fk_SESSIONS_has_MODULES_SESSIONS1_idx",
        using: "BTREE",
        fields: [
          { name: "sessionID" },
        ]
      },
    ]
  });
};
