import React, { useState, useContext } from "react"
import { Link, useParams, useHistory } from "react-router-dom"
import { UserContext } from "./UserContext"
import { NameContext } from "./components/NameContext"

function Visitor() {
	let { sessionCode } = useParams()
	const { isLoggedIn, setIsLoggedIn } = useContext(UserContext)
	const { setUsername } = useContext(NameContext)

	const history = useHistory()

	const [code, setCode] = useState(sessionCode)
	const [codeError, setCodeError] = useState("")

	const [nickname, setNickname] = useState("")
	const [nicknameError, setNicknameError] = useState("")

	const [gamePinError, setGamePinError] = useState(false)

	async function handleEnter(e) {
		e.preventDefault()

		setGamePinError(false)
		let _nicknameError = ""
		let _codeError = ""

		if (!isLoggedIn) {
			if (nickname.length < 2 || nickname.length > 255) {
				_nicknameError =
					"Your nickname has to be at least 3 characters long and 255 at maximum"
			}
		}
		if (code === "" || code === undefined) {
			_codeError = "Please enter your Code to join"
		}

		if (_nicknameError === "" && _codeError === "") {
			// NOCH NICHT VOLLSTÄNDIG IMPLEMENTIERT
			await fetch("https://coolaboration.tk/graphql", {
				method: "POST",
				body: JSON.stringify({
					query: `
                            mutation {
                                joinSession(code: "${code}", guestUsername: "${nickname}") {
                                    sessionName,
                                    moduleId,
                                    ${isLoggedIn ? "" : "guestToken"}
                                }
                            }
                    `,
				}),
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + window.localStorage.getItem("token"),
				},
			})
				.then((res) => {
					if (res.status !== 200 && res.status !== 201 && res.status !== 500) {
						throw new Error("Network error!")
					}
					return res.json()
				})
				.then((resData) => {
					console.log(resData)
					if (!isLoggedIn) {
						setUsername(nickname)
						localStorage.setItem("token", resData.data.joinSession.guestToken)
					}

					if (resData.errors) {
						setGamePinError(true)
					} else {
						history.push(
							"/session/" + resData.data.joinSession.moduleId + "/" + code
						)
					}
				})
				.catch((err) => {
					console.log(err)
				})
		}

		setCodeError(_codeError)
		setNicknameError(_nicknameError)
	}

	return (
		<div>
			<div className="d-flex flex-column min-vh-100 justify-content-center align-items-center ">
				<img
					src="https://cdn.discordapp.com/attachments/804262652548874269/819507982160232468/coolaborationLogo.png"
					alt="Logo"
					width="auto"
					height="34"
				/>
				<form class="container custom-form-400 mt-3">
					{gamePinError && (
						<div className="alert alert-danger" role="alert">
							You either joined this session already or it doesn't exist
						</div>
					)}
					<div className="mb-3">
						<label
							htmlFor="code"
							className="form-label"
							area-describedby="codeHelp"
						>
							Game PIN
						</label>
						<input
							type="text"
							className={
								codeError !== "" ? "form-control is-invalid" : "form-control"
							}
							id="code"
							onChange={(e) => setCode(e.target.value)}
							value={code}
						/>
						<div className={codeError !== "" ? "invalid-feedback" : "hidden"}>
							{codeError}
						</div>
						<div id="codeHelp" className="form-text">
							Ask the creator of the session for this code
						</div>
					</div>
					{!isLoggedIn && (
						<div className="mb-3">
							<label htmlFor="nickname" className="form-label">
								Nickname
							</label>
							<input
								type="text"
								className={
									nicknameError !== ""
										? "form-control is-invalid"
										: "form-control"
								}
								id="nickname"
								onChange={(e) => setNickname(e.target.value)}
								value={nickname}
							/>
							<div
								className={nicknameError !== "" ? "invalid-feedback" : "hidden"}
							>
								{nicknameError}
							</div>
						</div>
					)}
					<button
						type="submit"
						className="btn btn-primary btn-block w-100"
						onClick={handleEnter}
					>
						Enter
					</button>
					<div className="mt-2 d-flex justify-content-between">
						<Link to="/signin">Sign in here</Link>
						<Link to="/signup">Sign up here</Link>
					</div>
				</form>
			</div>
		</div>
	)
}

export default Visitor
