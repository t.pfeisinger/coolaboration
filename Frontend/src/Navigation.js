import React, { useContext } from "react"
import { Link } from "react-router-dom"
import { UserContext } from "./UserContext"

function Navigation() {
	const { isLoggedIn, setIsLoggedIn } = useContext(UserContext)

	function signout() {
		fetch("https://coolaboration.tk/graphql", {
			method: "POST",
			body: JSON.stringify({
				query: `
                    query {
                        logout
                    }
            `,
			}),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + window.localStorage.getItem("token"),
			},
		})
			.then((res) => {
				if (res.status !== 200 && res.status !== 201 && res.status !== 500) {
					throw new Error("Network error!")
				}
				return res.json()
			})
			.then((resData) => {
				console.log(resData)
			})
			.catch((err) => {
				console.log(err)
			})
		setIsLoggedIn(false)
	}

	return (
		<nav className="navbar navbar-expand-lg navbar-light bg-light">
			<div className="container-fluid d-flex justify-content-between">
				<Link className="navbar-brand me-auto" to="/dashboard">
					<img
						src="https://cdn.discordapp.com/attachments/804262652548874269/819507982160232468/coolaborationLogo.png"
						alt="Logo"
						width="auto"
						height="24"
					/>
				</Link>
				<button
					className="navbar-toggler"
					type="button"
					data-bs-toggle="collapse"
					data-bs-target="#navbarText"
					aria-controls="navbarText"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span className="navbar-toggler-icon"></span>
				</button>
				<div
					className="collapse navbar-collapse justify-content-end"
					id="navbarText"
				>
					<ul className="navbar-nav">
						<li className="nav-item">
							<Link className="nav-link" to="/dashboard/settings">
								Settings
							</Link>
						</li>
						<li className="nav-item">
							<Link className="nav-link" onClick={signout} to="/">
								Logout
							</Link>
						</li>
					</ul>
				</div>
			</div>
		</nav>
	)
}

export default Navigation
