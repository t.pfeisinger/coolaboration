import {
  Switch,
  Route,
  useRouteMatch
} from "react-router-dom";
import { React } from 'react';
import Navigation from './Navigation';
import Settings from './Settings';
import SessionList from "./SessionList";



function Dashboard() {    


    let match = useRouteMatch();
    return (
        <div className="Dashboard">
            <Navigation></Navigation>
            <Switch>
                <Route path={`${match.path}/settings`}>
                    <Settings />
                </Route>
                <Route path={match.path}>
                    <SessionList />
                </Route>
            </Switch>
        </div>
    );
}

export default Dashboard;