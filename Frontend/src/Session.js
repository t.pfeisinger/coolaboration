import React from 'react'
import { Link, useRouteMatch, Switch, Route, useParams } from 'react-router-dom'
import Chat from './Chat'

function Session() {
    let { moduleId, gameCode } = useParams();
    // check if the gameCode is valid 

    function renderRightModule() {
        switch(Number(moduleId)) {
            case 1:
                return <Chat gameCode={gameCode} />
            default:
                return <div>This doesn't exist yet</div>
        }
    }

    return (
        <div class="Session">
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div className="container-fluid d-flex justify-content-between">
                        <Link className="navbar-brand me-auto" to="/dashboard"><img src="https://cdn.discordapp.com/attachments/804262652548874269/819507982160232468/coolaborationLogo.png" alt="Logo" width="auto" height="24"/></Link>
                    </div>
            </nav>
            <>
            { 
                renderRightModule()

                
            }
            </>
        </div>
    )
}

export default Session;