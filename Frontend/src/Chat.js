import React, { useState, useEffect, useRef, useContext } from "react"
import { ListGroup, Form, Col, Button, Row } from "react-bootstrap"
import socketio from "socket.io-client"
import { NameContext } from "./components/NameContext"

function Chat({ gameCode }) {
	const socket = socketio.connect("https://coolaboration.tk")

	const [messages, setMessages] = useState([])
	const messageFeedRef = useRef()
	const { name } = useContext(NameContext)
	const messageRef = useRef("")

	useEffect(() => {


		// load old messages
		fetch("https://coolaboration.tk/graphql", {
			method: "POST",
			body: JSON.stringify({
				query: `
						query {
							getChatHistory (sessCode: "${gameCode}") {
								name,
								message,
								timestamp
							}
						}
				`,
			}),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + window.localStorage.getItem("token"),
			},
		})
		.then((res) => res.json())
		.then((resData) => {
			setMessages(resData.data.getChatHistory)
			window.scrollTo(0, document.body.scrollHeight)
		})
		.catch((err) => console.log(err))

		socket.on("message", ({ message, name, timestamp }) => {
			setMessages((messages) => [...messages, { message, name, timestamp }])

			window.scrollTo(0, document.body.scrollHeight)
		})

		socket.emit("joinRoom", { sessionCode: gameCode });

		return () => {
			socket.off("message")
		}
	}, [])


	const handleSend = async (e) => {
		e.preventDefault()

		socket.emit("message", {
			message: messageRef.current.value,
			name,
			timestamp: new Date(),
			sessionCode: gameCode,
		})

		await fetch("https://coolaboration.tk/graphql", {
			method: "POST",
			body: JSON.stringify({
				query: `
						mutation {
							addChatEntery(chatEntery: {sessCode: "${gameCode}", data: "${messageRef.current.value}", timestamp: "${new Date()}"}) 
						}
				`,
			}),
			headers: {
				"Content-Type": "application/json",
				Authorization: "Bearer " + window.localStorage.getItem("token"),
			},
		})
		.then(res => res.json())
		.then((resData) => {
			console.log(resData)
		})
		.catch((err) => console.log(err));

		messageRef.current.value = ""
		messageRef.current.focus()
	}

	return (
		<div class="container" ref={messageFeedRef}>
			<ListGroup className="Chat" as="ul">
				{messages.map((message) => (
					<ListGroup.Item
						as="li"
						active={message.name === name ? true : false}
						className="d-flex justify-content-between align-items-start"
						key={message.name + message.message + message.timestamp}
					>
						<div className="me-auto">
							<strong className="d-block">
								{message.name === name.name ? `You (${name})` : message.name}
							</strong>
							<span>{message.message}</span>
						</div>
						<span className="badge rounded-pill" style={{ color: "black" }}>
							{new Date(message.timestamp).toLocaleTimeString()}
						</span>
					</ListGroup.Item>
				))}
			</ListGroup>
			<Form onSubmit={handleSend} className="Sender mb-2">
				<Row>
					<Col sm={9} className="mt-2">
						<Form.Control type="text" ref={messageRef}></Form.Control>
					</Col>
					<Col sm={3} className="mt-2">
						<Button
							variant="primary"
							type="submit"
							style={{ width: "100%" }}
							block
						>
							Send
						</Button>
					</Col>
				</Row>
			</Form>
		</div>
	)
}

export default Chat
