const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('SESSION_INFOS', {
    sessionID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'SESSIONS',
        key: 'sessionID'
      }
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(512),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'SESSION_INFOS',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sessionID" },
        ]
      },
      {
        name: "fk_SESSION_INFOS_SESSIONS1_idx",
        using: "BTREE",
        fields: [
          { name: "sessionID" },
        ]
      },
    ]
  });
};
