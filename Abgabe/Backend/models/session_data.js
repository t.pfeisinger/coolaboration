const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('SESSION_DATA', {
    sessionDataID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    data: {
      type: DataTypes.STRING(512),
      allowNull: false
    },
    sessionID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'SESSIONS',
        key: 'sessionID'
      }
    },
    moduleID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'SESSIONS',
        key: 'moduleID'
      }
    },
    timestamp: {
      type: DataTypes.DATE(6),
      allowNull: false,
      unique: "timestamp_UNIQUE"
    },
    action: {
      type: DataTypes.STRING(15),
      allowNull: false,
      references: {
        model: 'E_ACTIONS',
        key: 'action'
      }
    },
    sessionUsername: {
      type: DataTypes.STRING(255),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'SESSION_DATA',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "sessionDataID" },
        ]
      },
      {
        name: "timestamp_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "timestamp" },
        ]
      },
      {
        name: "fk_SESSION_DATA_SESSIONS1_idx",
        using: "BTREE",
        fields: [
          { name: "sessionID" },
          { name: "moduleID" },
        ]
      },
      {
        name: "fk_SESSION_DATA_E_ACTIONS1_idx",
        using: "BTREE",
        fields: [
          { name: "action" },
        ]
      },
    ]
  });
};
