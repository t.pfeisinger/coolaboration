module.exports = `
    input SessionInput {
        name: String!
        desc: String!
        moduleId: Int!
    }
    input SessionState {
        state: String!
        code: String!
    }
    type SessionCreated {
        sessionId: Int!
        sessionCode: String!
        moduleName: String!
    }
    type Session {
        sessionId: Int!
        name: String!
        desc: String!
        sessionCode: String!
        sessionState: String!
        currentRole: String!
        moduleName: String!
        moduleId: Int!
    }
    type SessionJoined {
        sessionName: String!
        moduleId: Int!
        guestToken: String
    }
`;