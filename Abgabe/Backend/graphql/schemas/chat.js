module.exports = (`
    type ChatData {
        name: String!
        message: String!
        timestamp: String!
    }

    input ChatEntery {
        sessCode: String!
        data: String!
        timestamp: String!
    }
`);