const { buildSchema } = require('graphql');
const loginSchema = require('./login');
const regSchema = require('./registration');
const resetPw = require('./resetPw');
const sessions = require('./sessions');
const modules = require('./module');
const chat = require('./chat');

module.exports = buildSchema(
    loginSchema +
    regSchema +
    resetPw +
    sessions +
    modules +
    chat +
    `
    type RootQuery {
        getUsers: String!
        login(email: String!, password: String!): AuthData!
        resendMail(email: String!): RegData!
        passwdChangeMail: RegData!
        logout: Int!
        testToken: String!
        resetPw(email: String!): PwResetReturn!
        getAllSessions: [Session]!
        getAllModules: [Module]!
        getChatHistory(sessCode: String!): [ChatData]!
    }

    
    type RootMutation {
        createUser(userInput: UserInput!): RegData!
        changePassword(password: String!): AuthData!
        deleteUser(password: String!): Int!
        createSession(sessionInput: SessionInput!): SessionCreated!
        changeSessionState(sessionState: SessionState!): String!
        joinSession(code: String!, guestUsername: String): SessionJoined!
        deleteSession(sessionId: Int!): Int!
        leaveSession(sessionId: Int!): Int!
        addChatEntery(chatEntery: ChatEntery!): Int!
    }

    schema {
        query: RootQuery
        mutation: RootMutation
    }
`);