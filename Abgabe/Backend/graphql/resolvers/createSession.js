const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const sendMail = require('../../helperFuncs/sendMail');
const verContent = require('../../helperFuncs/mailcontentVerMail');
const createSessionCode = require('../../helperFuncs/createSessionCode');
const SESSION_INFOS = require('../../models/session_infos')(sequelize, DataTypes);
const SESSIONS = require('../../models/sessions')(sequelize, DataTypes);
const USERS_SESSIONS_JT = require('../../models/users_sessions_jt')(sequelize, DataTypes);
const MODULE_INFO = require('../../models/module_infos')(sequelize, DataTypes);

async function asyncForEach(array, callback) {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
}

module.exports = {
    createSession: async (args, req) => {
        if (!req.isAuth) {
            throw new Error("Not Authenticated");
        }
        if (!req.userId) {
            throw new Error("No User");
        }
        try {
            let code = createSessionCode();

            const sessions = await SESSIONS.build({
                stateLabel: "ACTIVE",
                code: code,
                moduleID: args.sessionInput.moduleId
            });

            const session_result = await sessions.save();

            let sessID = session_result.sessionID;

            //throw new Error(sessID);


            const sessionInfo = await SESSION_INFOS.build({
                sessionID: sessID,
                name: args.sessionInput.name,
                description: args.sessionInput.desc
            });

            console.log(sessionInfo);
            await sessionInfo.save();


            const usersSessionJT = await USERS_SESSIONS_JT.build({
                userID: req.userId,
                sessionID: sessID,
                roleLabel: "OWNER",
                moduleID: args.sessionInput.moduleId
            })
            console.log(usersSessionJT)

            await usersSessionJT.save();
            
            const moduleInfo = await MODULE_INFO.findOne({
                where: { moduleID: args.sessionInput.moduleId }
            });
            console.log(moduleInfo);
            return {
                sessionId: sessID,
                sessionCode: code,
                moduleName: moduleInfo.name
            };

        }
        catch (err) { throw err }
    },
    getAllSessions: async (args, req) => {
        if(!req.isAuth){
            throw new Error("Not Authenticated");
        }
        if(!req.userId){
            throw new Error("No User");
        }
        try {
            const sessionIds = await USERS_SESSIONS_JT.findAll({ where: { userID: req.userId } });
            let sessions = [];
            let sessionInfos = [];
            let modules = [];
            await asyncForEach(sessionIds, async (Id) => {
                const sess = await SESSIONS.findOne({ where: { sessionID: Id.dataValues.sessionID } });
                const sessInfo = await SESSION_INFOS.findOne({ where: { sessionID: Id.dataValues.sessionID } });
                const mod = await MODULE_INFO.findOne({ where: { moduleID: Id.dataValues.moduleID } });
                sessions.push(sess.dataValues);
                sessionInfos.push(sessInfo.dataValues);
                modules.push(mod.dataValues);
            });
            //console.log(sessions);
            //console.log(sessionInfos);
            //console.log(sessionIds);
            //console.log(modules);
            let retElement = [];
            for (let i = 0; i < sessions.length; i++) {
                const item = {
                    sessionId: sessions[i].sessionID,
                    name: sessionInfos[i].name,
                    desc: sessionInfos[i].description,
                    sessionCode: sessions[i].code,
                    sessionState: sessions[i].stateLabel,
                    currentRole: sessionIds[i].dataValues.roleLabel,
                    moduleName: modules[i].name,
                    moduleId: sessions[i].moduleID
                };
                retElement.push(item);
            }
            return retElement;
        } catch (error) {
            throw error;
        }
    }
}
/*
sessionId: Int!
        name: String!
        desc: String!
        sessionCode: String!
        sessionState: String!
        currentRole: String!
        moduleName: String!*/