const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const Session = require('../../models/sessions')(sequelize, DataTypes);
const SessData = require('../../models/session_data')(sequelize, DataTypes)

module.exports = {
    addChatEntery: async(args, req) => {
        let name;
        let user; 
        let sess;
        if (!req.isAuth) {
            throw new Error("Not Authenticated");
        }
        if (!req.userId) {
            name = 'GUESTUSER'
        }
        else{
            user = await User.findOne( { where: { userID: req.userId } } );
            name = user.username;
        }
        try {
            sess = await Session.findOne( { where: { code: args.chatEntery.sessCode } } );
        } catch (error) {
            throw error;
        }
        const sessData = await SessData.build({
            data: args.chatEntery.data,
            sessionID: sess.sessionID,
            moduleID: sess.moduleID,
            timestamp: args.chatEntery.timestamp,
            action: 'TRANSMIT',
            sessionUsername: name
        });

        const dataResult = await sessData.save();

        return dataResult.sessionDataID;
    },
    getChatHistory: async(args, req) => {
        if (!req.isAuth) {
            throw new Error("Not Authenticated");
        }
        try {
            let arr = [];
            const sess = await Session.findOne( { where: { code: args.sessCode } } );
            const sessData = await SessData.findAll( { where: { sessionID: sess.sessionID, moduleID: sess.moduleID } } );

            sessData.forEach(element => {
                let item = {
                    name: element.dataValues.sessionUsername,
                    message: element.dataValues.data,
                    timestamp: new Date(element.dataValues.timestamp).toISOString() 
                }
                arr.push(item);
            });

            return arr;
        } catch (error) {
            throw error;
        }
    }
};