create table BLACKLISTED_TOKENS
(
    tokenId   int auto_increment,
    token     varchar(255) not null,
    tokenDate date         not null,
    constraint tokenId_UNIQUE
        unique (tokenId),
    constraint token_UNIQUE
        unique (token)
);

alter table BLACKLISTED_TOKENS
    add primary key (tokenId);

create table E_ACCOUNT_STATES
(
    stateLabel varchar(15) not null
        primary key
);

create table E_ACTIONS
(
    action varchar(15) not null,
    constraint action_UNIQUE
        unique (action)
);

alter table E_ACTIONS
    add primary key (action);

create table E_MODULE_STATES
(
    stateLabel varchar(15) not null
        primary key
);

create table E_SESSION_STATES
(
    stateLabel varchar(15) not null
        primary key
);

create table E_USER_ROLE
(
    roleLabel varchar(15) not null
        primary key
);

create table MODULES_BT
(
    moduleID   int auto_increment,
    stateLabel varchar(15)    not null,
    code       varchar(16384) not null,
    constraint moduleID_UNIQUE
        unique (moduleID),
    constraint fk_MODULES_E_MODULE_STATES1
        foreign key (stateLabel) references E_MODULE_STATES (stateLabel)
);

create index fk_MODULES_E_MODULE_STATES1_idx
    on MODULES_BT (stateLabel);

alter table MODULES_BT
    add primary key (moduleID);

create table MODULE_INFOS
(
    name        varchar(50)  not null,
    description varchar(255) not null,
    moduleID    int          not null
        primary key,
    constraint fk_MODULE__INFOS_MODULES1
        foreign key (moduleID) references MODULES_BT (moduleID)
);

create index fk_MODULE__INFOS_MODULES1_idx
    on MODULE_INFOS (moduleID);

create table SESSIONS
(
    sessionID  int auto_increment,
    stateLabel varchar(15) not null,
    code       varchar(15) not null,
    moduleID   int         not null,
    primary key (sessionID, moduleID),
    constraint code_UNIQUE
        unique (code),
    constraint sessionID_UNIQUE
        unique (sessionID),
    constraint fk_SESSIONS_E_SESSION_STATES1
        foreign key (stateLabel) references E_SESSION_STATES (stateLabel),
    constraint fk_SESSIONS_MODULES_BT1
        foreign key (moduleID) references MODULES_BT (moduleID)
);

create index fk_SESSIONS_E_SESSION_STATES1_idx
    on SESSIONS (stateLabel);

create index fk_SESSIONS_MODULES_BT1_idx
    on SESSIONS (moduleID);

create table SESSION_DATA
(
    sessionDataID   int auto_increment
        primary key,
    data            varchar(512) not null,
    sessionID       int          not null,
    moduleID        int          not null,
    timestamp       datetime(6)  not null,
    action          varchar(15)  not null,
    sessionUsername varchar(255) not null,
    constraint timestamp_UNIQUE
        unique (timestamp),
    constraint fk_SESSION_DATA_E_ACTIONS1
        foreign key (action) references E_ACTIONS (action),
    constraint fk_SESSION_DATA_SESSIONS1
        foreign key (sessionID, moduleID) references SESSIONS (sessionID, moduleID)
);

create index fk_SESSION_DATA_E_ACTIONS1_idx
    on SESSION_DATA (action);

create index fk_SESSION_DATA_SESSIONS1_idx
    on SESSION_DATA (sessionID, moduleID);

create table SESSION_INFOS
(
    sessionID   int          not null
        primary key,
    name        varchar(255) not null,
    description varchar(512) not null,
    constraint fk_SESSION_INFOS_SESSIONS1
        foreign key (sessionID) references SESSIONS (sessionID)
);

create index fk_SESSION_INFOS_SESSIONS1_idx
    on SESSION_INFOS (sessionID);

create table USERS_BT
(
    userID        int auto_increment,
    username      varchar(255) not null,
    password_hash varchar(255) null,
    emailAddress  varchar(255) not null,
    stateLabel    varchar(15)  not null,
    constraint emailAddress_UNIQUE
        unique (emailAddress),
    constraint userID_UNIQUE
        unique (userID),
    constraint username_UNIQUE
        unique (username),
    constraint fk_USERS_BT_E_ACCOUNT_STATES1
        foreign key (stateLabel) references E_ACCOUNT_STATES (stateLabel)
);

create index fk_USERS_BT_E_ACCOUNT_STATES1_idx
    on USERS_BT (stateLabel);

alter table USERS_BT
    add primary key (userID);

create table ADMINS
(
    userID int not null
        primary key,
    constraint fk_ADMINS_USERS_BT1
        foreign key (userID) references USERS_BT (userID)
);

create table USERS_SESSIONS_JT
(
    userID    int         not null,
    roleLabel varchar(15) not null,
    sessionID int         not null,
    moduleID  int         not null,
    primary key (userID, sessionID, moduleID),
    constraint fk_USERS_BT_has_SESSIONS_USERS_BT1
        foreign key (userID) references USERS_BT (userID),
    constraint fk_USERS_SESSIONS_JT_E_USER_ROLE1
        foreign key (roleLabel) references E_USER_ROLE (roleLabel),
    constraint fk_USERS_SESSIONS_JT_SESSIONS1
        foreign key (sessionID, moduleID) references SESSIONS (sessionID, moduleID)
);

create index fk_USERS_BT_has_SESSIONS_USERS_BT1_idx
    on USERS_SESSIONS_JT (userID);

create index fk_USERS_SESSIONS_JT_E_USER_ROLE1_idx
    on USERS_SESSIONS_JT (roleLabel);

create index fk_USERS_SESSIONS_JT_SESSIONS1_idx
    on USERS_SESSIONS_JT (sessionID, moduleID);

create table USER_INFOS
(
    firstName varchar(50) not null,
    lastName  varchar(50) not null,
    userID    int         not null
        primary key,
    constraint fk_USER_INFOS_USERS_BT1
        foreign key (userID) references USERS_BT (userID)
);

create index fk_USER_INFOS_USERS_BT1_idx
    on USER_INFOS (userID);

