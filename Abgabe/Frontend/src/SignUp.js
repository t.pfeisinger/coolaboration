import React, { useState } from "react"
import { Link } from "react-router-dom"

function SignUp() {
	const [email, setEmail] = useState("")
	const [username, setUsername] = useState("")
	const [emailError, setEmailError] = useState("")
	const [usernameError, setUsernameError] = useState("")
	const [getsEmail, setGetsEmail] = useState(false)

	const handleRegister = async () => {
		setEmailError("")
		setUsernameError("")

		let _emailError = validateEmail()
		let _usernameError = validateUsername()

		if (_emailError === "" && _usernameError === "") {
			//console.log("Erfolgreich: " + email + ":" + username);
			//oage backen d email versend methode
			await fetch("https://coolaboration.tk/graphql", {
				method: "POST",
				body: JSON.stringify({
					query: `
                            mutation {
                                createUser(userInput: {email: "${email}", username: "${username}"}) {
                                    userId,
                                    token,
                                    expiration
                                }
                            }
                    `,
				}),
				headers: {
					"Content-Type": "application/json",
				},
			})
				.then((res) => {
					if (res.status !== 200 && res.status !== 201 && res.status !== 500) {
						throw new Error("Network error!")
					}
					return res.json()
				})
				.then((resData) => {
					console.log(resData)
					if (resData.errors === undefined) {
						localStorage.setItem("token", resData.data.createUser.token)
						setGetsEmail(true)
					} else {
						for (let i of resData.errors) {
							if (i.message.includes("Email")) {
								_emailError = i.message
							} else if (i.message.includes("Username")) {
								_usernameError = i.message
							}
						}
					}
				})
				.catch((err) => {
					console.log(err)
				})
		}

		setEmailError(_emailError)
		setUsernameError(_usernameError)
	}

	const validateEmail = () => {
		if (email !== "" && email.includes("@") && email.length <= 255) {
			if (false) {
				// email already exists
				return "Email already in use"
			}
		} else {
			return "Invalid email"
		}

		return ""
	}
	const validateUsername = () => {
		if (username !== "" && username.length <= 255) {
			if (false) {
				// username already exsists
				return "Username already in use"
			}
		} else {
			return "Invalid username"
		}

		return ""
	}

	return (
		<div className="d-flex flex-column min-vh-100 justify-content-center align-items-center">
			<h1 className="mb-5">Sign Up</h1>
			<form
				className="Login container custom-form"
				onSubmit={(e) => e.preventDefault()}
			>
				<div className="mb-3">
					<label htmlFor="email" className="form-label">
						Email address
					</label>
					<input
						type="text"
						className={
							emailError !== "" ? "form-control is-invalid" : "form-control"
						}
						id="email"
						onChange={(e) => setEmail(e.target.value)}
						value={email}
					/>
					<div className={emailError !== "" ? "invalid-feedback" : "hidden"}>
						{emailError}
					</div>
				</div>
				<div className="mb-3">
					<label
						htmlFor="password"
						className="form-label"
						aria-describedby="usernameHelp"
					>
						Username
					</label>
					<input
						type="text"
						className={
							usernameError !== "" ? "form-control is-invalid" : "form-control"
						}
						id="password"
						onChange={(e) => setUsername(e.target.value)}
						value={username}
					/>
					<div className={usernameError !== "" ? "invalid-feedback" : "hidden"}>
						{usernameError}
					</div>
					<div id="usernameHelp" className="form-text">
						This will be the name that other people see
					</div>
				</div>
				{getsEmail && (
					<div className="alert alert-success" role="alert">
						A verfication email was sent to you. Please click on the link in it
					</div>
				)}
				<button
					type="submit"
					className="btn btn-primary btn-block w-100"
					onClick={handleRegister}
				>
					Sign up
				</button>
				<div className="mt-2">
					Already have an account? <Link to="/signin">Sign in here</Link>
				</div>
			</form>
		</div>
	)
}

export default SignUp
