import React, { useEffect, useState } from "react"
import Popover from "react-bootstrap/Popover"
import OverlayTrigger from "react-bootstrap/OverlayTrigger"
import Modal from "react-bootstrap/Modal"
import Button from "react-bootstrap/Button"
import Overlay from "react-bootstrap/Overlay"
import Tooltip from "react-bootstrap/Tooltip"

function AddSessionModal({ show, handleClose, handleCreate }) {
	const [newSession, setNewSession] = useState({ name: "", type: "1" })
	const [newSessionError, setNewSessionError] = useState("")

	const [availableModules, setAvailableModules] = useState([])

	useEffect(() => {
		async function getAllModules() {
			await fetch("https://coolaboration.tk/graphql", {
				method: "POST",
				body: JSON.stringify({
					query: `
                        query {
                            getAllModules {
                                moduleID,
                                name
                            }
                        } `,
				}),
				headers: {
					"Content-Type": "application/json",
					Authorization: "Bearer " + window.localStorage.getItem("token"),
				},
			})
				.then((res) => res.json())
				.then((res) => {
					setAvailableModules(res.data.getAllModules)
				})
				.catch((error) => console.log(error))
		}

		getAllModules()
	}, [])

	async function createSession() {
		if (newSession.name === "") {
			setNewSessionError("Can't be empty")
		} else {
			handleCreate(newSession)

			setNewSession({ name: "", type: "1" })
			handleClose()
			setNewSessionError("")
		}
	}

	return (
		<Modal show={show} onHide={handleClose}>
			<Modal.Header>
				<Modal.Title>Create Session</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<div className="mb-3">
					<label htmlFor="name" className="form-label">
						Name
					</label>
					<input
						value={newSession.name}
						className={
							newSessionError !== ""
								? "form-control is-invalid"
								: "form-control"
						}
						type="text"
						id="name"
						onChange={(e) =>
							setNewSession({ ...newSession, name: e.target.value })
						}
					/>
					<div
						className={newSessionError !== "" ? "invalid-feedback" : "hidden"}
					>
						{newSessionError}
					</div>
				</div>
				<div className="mb-3">
					<label htmlFor="type" className="form-label">
						Module type
					</label>
					<select
						value={newSession.type}
						className="form-select"
						aria-label="type"
						id="type"
						onChange={(e) =>
							setNewSession({ ...newSession, type: e.target.value })
						}
					>
						{availableModules.map((module) => {
							return (
								<option
									key={Number(module.moduleID)}
									value={Number(module.moduleID)}
								>
									{module.name}
								</option>
							)
						})}
					</select>
				</div>
			</Modal.Body>
			<Modal.Footer>
				<Button variant="secondary" onClick={handleClose}>
					Close
				</Button>
				<Button variant="primary" onClick={createSession}>
					Create session
				</Button>
			</Modal.Footer>
		</Modal>
	)
}

export default AddSessionModal
