import React from "react"
import ReactDOM from "react-dom"
import App from "./App"
import { ApolloClient, InMemoryCache } from "@apollo/client"
import { ApolloProvider } from "@apollo/client/react"

const client = new ApolloClient({
	uri: "https://coolaboration.tk/graphql",
	cache: new InMemoryCache(),
})

ReactDOM.render(
	<ApolloProvider client={client}>
		<React.StrictMode>
			<App />
		</React.StrictMode>
	</ApolloProvider>,
	document.getElementById("root")
)
