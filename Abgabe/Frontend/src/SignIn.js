import React, { useState, useContext } from "react"
import { Link, useHistory } from "react-router-dom"
import { UserContext } from "./UserContext"

function SignIn({ justSetThePassword, setJustSetThePassword }) {
	const { isLoggedIn, setIsLoggedIn } = useContext(UserContext)
	const history = useHistory()

	const [email, setEmail] = useState("")
	const [password, setPassword] = useState("")
	const [error, setError] = useState(false)

	// useEffect(() => {
	//     setJustSetThePassword(localStorage.getItem("justSetThePassword") !== null && localStorage.getItem("justSetThePassword") === true);
	//     localStorage.removeItem("justSetThePassword");
	// }, []);

	function handleLogin(e) {
		setError(false)

		if (email !== "" && password !== "") {
			fetch("https://coolaboration.tk/graphql", {
				method: "POST",
				body: JSON.stringify({
					query: `
                            query {
                                login(email: "${email}", password: "${password}") {
                                    userId,
                                    token
                                }
                            }
                    `,
				}),
				headers: {
					"Content-Type": "application/json",
				},
			})
				.then((res) => {
					if (res.status !== 200 && res.status !== 201 && res.status !== 500) {
						throw new Error("Network error!")
					}
					return res.json()
				})
				.then((loginData) => {
					if (loginData.errors === undefined) {
						localStorage.setItem("token", loginData.data.login.token)
						setIsLoggedIn(true)
						history.push("/dashboard")
					} else {
						setError(true)
						console.log("FACK")
					}
				})
				.catch((err) => {
					console.log(err)
				})
		}

		setJustSetThePassword(false)
	}

	return (
		<div className="d-flex flex-column min-vh-100 justify-content-center align-items-center">
			<h1 className="mb-5">Sign In</h1>
			<form
				className="Login container custom-form needs-validation"
				onSubmit={(e) => e.preventDefault()}
			>
				{justSetThePassword && (
					<div className="alert alert-success" role="alert">
						Your password was set. Please login now
					</div>
				)}
				<div className="mb-3">
					<label htmlFor="email" className="form-label">
						Email address
					</label>
					<input
						type="text"
						className="form-control"
						id="email"
						onChange={(e) => setEmail(e.target.value)}
						value={email}
					/>
					<div className="valid-feedback"></div>
				</div>
				<div className="mb-3">
					<label
						htmlFor="password"
						className="form-label"
						aria-describedby="passwordHelp"
					>
						Password
					</label>
					<input
						type="password"
						className="form-control"
						id="password"
						onChange={(e) => setPassword(e.target.value)}
						value={password}
					/>
					<div id="passwordHelp" className="form-text float-right">
						<Link to="/forgot-password">Forgot password?</Link>
					</div>
				</div>
				{error && (
					<div className="alert alert-danger" role="alert">
						Login credentials are incorrect
					</div>
				)}
				<button
					type="submit"
					className="btn btn-primary btn-block w-100"
					onClick={handleLogin}
				>
					Sign in
				</button>
				<div className="mt-2">
					Don't have an account? <Link to="/signup">Create one here</Link>
				</div>
			</form>
		</div>
	)
}

export default SignIn
