const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MODULE_INFOS', {
    name: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    description: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    moduleID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'MODULES_BT',
        key: 'moduleID'
      }
    }
  }, {
    sequelize,
    tableName: 'MODULE_INFOS',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "moduleID" },
        ]
      },
      {
        name: "fk_MODULE__INFOS_MODULES1_idx",
        using: "BTREE",
        fields: [
          { name: "moduleID" },
        ]
      },
    ]
  });
};
