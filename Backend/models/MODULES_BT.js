const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('MODULES_BT', {
    moduleID: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    stateLabel: {
      type: DataTypes.STRING(15),
      allowNull: false,
      references: {
        model: 'E_MODULE_STATES',
        key: 'stateLabel'
      }
    },
    code: {
      type: DataTypes.STRING(16384),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'MODULES_BT',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "moduleID" },
        ]
      },
      {
        name: "moduleID_UNIQUE",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "moduleID" },
        ]
      },
      {
        name: "fk_MODULES_E_MODULE_STATES1_idx",
        using: "BTREE",
        fields: [
          { name: "stateLabel" },
        ]
      },
    ]
  });
};
