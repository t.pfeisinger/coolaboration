const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('USERS_SESSIONS_JT', {
    userID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'USERS_BT',
        key: 'userID'
      }
    },
    roleLabel: {
      type: DataTypes.STRING(15),
      allowNull: false,
      references: {
        model: 'E_USER_ROLE',
        key: 'roleLabel'
      }
    },
    sessionID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'SESSIONS',
        key: 'sessionID'
      }
    },
    moduleID: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      references: {
        model: 'SESSIONS',
        key: 'moduleID'
      }
    }
  }, {
    sequelize,
    tableName: 'USERS_SESSIONS_JT',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "userID" },
          { name: "sessionID" },
          { name: "moduleID" },
        ]
      },
      {
        name: "fk_USERS_BT_has_SESSIONS_USERS_BT1_idx",
        using: "BTREE",
        fields: [
          { name: "userID" },
        ]
      },
      {
        name: "fk_USERS_SESSIONS_JT_E_USER_ROLE1_idx",
        using: "BTREE",
        fields: [
          { name: "roleLabel" },
        ]
      },
      {
        name: "fk_USERS_SESSIONS_JT_SESSIONS1_idx",
        using: "BTREE",
        fields: [
          { name: "sessionID" },
          { name: "moduleID" },
        ]
      },
    ]
  });
};
