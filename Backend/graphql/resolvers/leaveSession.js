const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const Session = require('../../models/sessions')(sequelize, DataTypes);
const UserSession = require('../../models/users_sessions_jt')(sequelize, DataTypes);

module.exports = {
    leaveSession: async (args, req) => {
        try {
            if (!req.isAuth) {
                throw new Error('Unauthenticated');
            }
            const user = await User.findOne({ where: { userID: req.userId } });

            if (!user) {
                throw new Error('User not found!');
            }

            const session = await Session.findOne({ where: { sessionID: args.sessionId } });

            if (!session) {
                throw new Error('Session not found!');
            }

            const usersession = await UserSession.findOne({ where: { sessionID: args.sessionId, userID: req.userId } });

            await usersession.destroy();

            return 1;
        }
        catch (err) {
            throw err;
        }
    }
};