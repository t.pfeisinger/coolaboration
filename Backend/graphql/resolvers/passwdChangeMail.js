const jwt = require("jsonwebtoken")
const sequelize = require("../../connection")
const DataTypes = require("sequelize").DataTypes
const User = require("../../models/users_bt")(sequelize, DataTypes)
const sendMail = require("../../helperFuncs/sendMail")
const mailcontentResetPw = require("../../helperFuncs/mailcontentResetPw")

module.exports = {
	passwdChangeMail: async (args, req) => {
		try {
			if (!req.isAuth) {
				throw new Error("Unauthenticated " + req.userId)
			}

			const user = await User.findOne({ where: { userID: req.userId } })

			if (!user) {
				throw new Error("User not found!")
			}

			/*
            const user = await User.build({ 
                username: existingUser.username,
                emailAddress: args.email,
                stateLabel: "REGISTERED",
            });
    
            const result = await user.save();
            */

			const token = jwt.sign(
				{ userId: user.userID, userState: user.stateLabel },
				"tokenkey",
				{
					expiresIn: "1h",
				}
			)

			sendMail(
				mailcontentResetPw("http://coolaboration.tk/change-password", token),
				"Change your Password",
				"Password Change request | Coolaboration",
				user.emailAddress
			)
			return { userId: user.userID, token: token, expiration: 1 }
		} catch (err) {
			throw err + " penis"
		}
	},
}
