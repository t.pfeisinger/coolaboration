const jwt = require('jsonwebtoken');
const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const bcrypt = require('bcryptjs');

module.exports = {
    login: async ({email, password}) => {
        try {
            const user = await User.findOne({ where: { emailAddress: email } });
            if(!user){
                throw new Error('User does not exist!');
            }
            const isEqual = await bcrypt.compare(password, user.password_hash);
            if(!isEqual){
                throw new Error('Password is incorrect!');
            }
            const token = jwt.sign({userId: user.userID, userstate: user.stateLabel}, 'tokenkey', {
                expiresIn: '14d'});
           
            return { userId: user.userID, token: token }
        }
        catch (err) { throw err }
    }
}
