const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const ModuleInfo = require('../../models/module_infos')(sequelize, DataTypes);

module.exports = {
    getAllModules: async(args, req) => {
        
        let arr = [];

        const moduleinfo = await  ModuleInfo.findAll();

        moduleinfo.forEach(element => {
            let item = 
            {
                moduleID: element.dataValues.moduleID,
                name: element.dataValues.name
            }
            arr.push(item);
        });

        return arr;
    }
};