const jwt = require('jsonwebtoken');
const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const SESSION_INFOS = require('../../models/session_infos')(sequelize, DataTypes);
const SESSIONS = require('../../models/sessions')(sequelize, DataTypes);
const USERS_SESSIONS_JT = require('../../models/users_sessions_jt')(sequelize, DataTypes);

module.exports = {
    joinSession: async (args, req) => {
        let session;
        let sessionInfo;
        try {
            session = await SESSIONS.findOne({ where: { code: args.code } });
            if (!session) {
                throw new Error("No valid Session code");
            }
            sessionInfo = await SESSION_INFOS.findOne({ where: { sessionID: session.sessionID } });
        } catch (error) {
            throw error;
        }
        if (session.stateLabel == "ACTIVE") {
            if (!req.isAuth) {
                if (!args.guestUsername) {
                    throw new Error('No Username defined!');
                }
                const guestToken = jwt.sign({ userState: 'GUEST' }, 'tokenkey', {
                    expiresIn: '14d'
                });
                return { sessionName: sessionInfo.name,
                    moduleId: session.moduleID,
                    guestToken: guestToken }
            }
            try {
                const user = await User.findOne({ where: { userID: req.userId } });
                if (!user) {
                    throw new Error('No valid User');
                }
                const usersSessionJT = await USERS_SESSIONS_JT.build({
                    userID: user.userID,
                    sessionID: session.sessionID,
                    roleLabel: "VIEWER",
                    moduleID: session.moduleID
                });
                await usersSessionJT.save();
                return {
                    sessionName: sessionInfo.name,
                    moduleId: session.moduleID
                }
            } catch (error) {
                throw error;
            }
        }
    }
}