const jwt = require('jsonwebtoken');
const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const bcrypt = require('bcryptjs');

module.exports = {
    changePassword: async (args, req) => {
        if(!req.isAuth){
            throw new Error('Unauthenticated');
        }
        
        const user = await User.findOne({ where: { userID: req.userId } });

        if(!user){
            throw new Error('User not found!');
        }
        const hashedPw = await bcrypt.hash(args.password, 12);

        try {
            await user.update({ password_hash: hashedPw });
        }
        catch (err) { console.log(err); }

        const token = jwt.sign({userId: user.userID, userstate: user.stateLabel}, 'tokenkey',
            { expiresIn: '14d' });
        
        return { userId: user.userID, token: token };
    }
};