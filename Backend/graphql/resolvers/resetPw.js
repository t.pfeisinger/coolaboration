const jwt = require("jsonwebtoken")
const sequelize = require("../../connection")
const DataTypes = require("sequelize").DataTypes
const User = require("../../models/users_bt")(sequelize, DataTypes)
const sendMail = require("../../helperFuncs/sendMail")
const mailcontentResetPw = require("../../helperFuncs/mailcontentResetPw")

module.exports = {
	resetPw: async (args) => {
		try {
			user = await User.findOne({ where: { emailAddress: args.email } })
			if (!user) {
				throw new Error("Email not existant")
			}
		} catch (err) {
			throw err
		}

		const token = jwt.sign(
			{ userId: user.userID, userState: user.stateLabel },
			"tokenkey",
			{
				expiresIn: "1h",
			}
		)

		sendMail(
			mailcontentResetPw("http://coolaboration.tk/change-password", token),
			"Change your Password",
			"Password Change request | Coolaboration",
			args.email
		)
		return { userId: user.userID, expiration: 1, token: token }
	},
}
