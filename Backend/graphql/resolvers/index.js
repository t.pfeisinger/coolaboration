const regResolver = require('./registration');
const logResolver = require('./login');
const changePw = require('./changePasswd');
const resendMail = require('./resendMail');
const passwdChangeMail = require('./passwdChangeMail');
const logout = require('./logout');
const testToken = require('./testToken');
const resetPw = require('./resetPw');
const delUser = require('./deleteUser');
const createSession = require('./createSession');
const joinSession = require('./joinSession');
const changeSessState = require('./changeSessionState');
const delSess = require('./deleteSession');
const leaveSession = require('./leaveSession');
const getAllModules = require('./getAllModules');

const rootResolver = {
  ...regResolver,
  ...logResolver,
  ...changePw,
  ...resendMail,
  ...passwdChangeMail,
  ...logout,
  ...testToken,
  ...resetPw,
  ...delUser,
  ...createSession,
  ...joinSession,
  ...changeSessState,
  ...delSess,
  ...leaveSession,
  ...getAllModules
};

module.exports = rootResolver;