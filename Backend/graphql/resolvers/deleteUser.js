const sequelize = require('../../connection');
const DataTypes = require('sequelize').DataTypes;
const User = require('../../models/users_bt')(sequelize, DataTypes);
const bcrypt = require('bcryptjs');

module.exports = {
    deleteUser: async(args, req) => {
        if(!req.isAuth){
            throw new Error('Unauthenticated');
        }
        const user = await User.findOne({ where: { userID: req.userId } });

        if(!user){
            throw new Error('User not found!');
        }

        const isEqual = await bcrypt.compare(args.password, user.password_hash);

        if(!isEqual)
        {
            throw new Error('Password is incorrect!');
        }
        const temp = user;
        await user.destroy();
        return {userId: temp.userID};
    }
};