const jwt = require("jsonwebtoken")
const sequelize = require("../../connection")
const DataTypes = require("sequelize").DataTypes
const User = require("../../models/users_bt")(sequelize, DataTypes)
const sendMail = require("../../helperFuncs/sendMail")
const verContent = require("../../helperFuncs/mailcontentVerMail")

module.exports = {
	resendMail: async (args) => {
		try {
			let existingUser = await User.findOne({
				where: { emailAddress: args.email },
			})
			if (!existingUser)
				throw new Error("Email is not available " + args.email + existingUser)

			console.log(existingUser.username, args.email)
			const user = await User.build({
				userID: existingUser.userID,
				username: existingUser.username,
				emailAddress: args.email,
				stateLabel: "REGISTERED",
			})

			const result = await user.save()

			const token = jwt.sign(
				{ userId: result.userID, userState: result.stateLabel },
				"tokenkey",
				{
					expiresIn: "1h",
				}
			)

			sendMail(
				verContent("http://coolaboration.tk/verify-email", token),
				"Please verify E-Mail-address",
				"Email Verfication | Coolaboration",
				args.email,
				"Verify"
			)

			return { userId: result.userID, token: token, expiration: 1 }
		} catch (err) {
			throw err
		}
	},
}
