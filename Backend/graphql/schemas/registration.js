module.exports = (`
type RegData {
    userId: ID!
    token: String!
    expiration: Int!
}

input UserInput {
    email: String!
    username: String!
}
`);